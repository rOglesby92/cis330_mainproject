We have decided to use QT platform for C++ to implement our user interface, after looking at some tutorials online it seems like a good fit for user 
input and output with buttons and visuals.

https://www.youtube.com/watch?v=FhV1ZEVNK08

This tutorial about creating a calculator has some good steps and examples how to make buttons that will output different results and store them

We can make use of these features for our game which revolves around getting user input from buttons and matching them to the given computer outputs

We will upload the code from the QT application onto this repo when we have some generated.